import java.util.Iterator;

public class main {
    public static void main(String[] args) {
        Integer[][]array =  {{1,2},{2,3,4,5,},{6,7},{8,9,10,11,12,}};
        Array2d <Integer> arrayIterator = new Array2d<Integer>(array);
        Iterator<Integer> iterator = arrayIterator.iterator();
        while(iterator.hasNext()) System.out.println(iterator.next());
    }
}

import java.util.Iterator;
import java.util.NoSuchElementException;

class Array2d<E> implements Iterable<E>{
private E[][] array;
public Array2d(E[][] array) {
        this.array = array;
        }

    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int i, j;
            public boolean hasNext() {
                for(int i = this.i; i< array.length; i++){
                    for(int j = this.j; j< array[i].length; j++){
                        return true;
                    }
                }
                return false;
            }

            public E next() {
                if(!hasNext())
                    throw new NoSuchElementException();
                E new_array = array[i][j];
                j++;
                for(int i = this.i; i< array.length; i++){
                    for(int j = (i == this.i ? this.j : 0); j< array[i].length; j++){
                        this.i = i;
                        this.j = j;
                        return new_array;
                    }
                }

                return new_array;
            }

            public void remove() {
                throw new UnsupportedOperationException();

            }
        };
    }
}
